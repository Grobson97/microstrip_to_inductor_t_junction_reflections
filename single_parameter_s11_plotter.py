import util.touchstone_tools as touchstone_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os

# Set the value for the inductor width to plot the data corresponding to this value.
target_inductor_width = 2.0
# directory = r"data"
directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\Inductor_microstrip_split_junction\extra_lossy_deembedded_data"
simulation_results = {"input_width": [], "inductor_width": [], "al_resistance": [], "mean_s11": [], "mean_s11_err": []}

# Select the file to plot:
filename = "microstrip_to_split_KID_inductor_shorter_line_param379.s3p"
file_path = directory + "\\" + filename

if ".s3p" in filename:

    # Read current touchstone file
    touchstoneFile = rf.Touchstone(file_path)
    # Get touchstone variables from comments
    variables = touchstoneFile.get_comment_variables()
    input_width = float(variables["Nb_width"][0])
    inductor_width = float(variables["inductor_width"][0])
    al_resistance = float(variables["Al_Rs"][0])

    # Create network from touchstone file
    network = rf.Network(file_path)
    port_1_mean_impedance = touchstone_tools.get_mean_port_impedance(
        file_path, "1"
    )
    port_2_mean_impedance = touchstone_tools.get_mean_port_impedance(
        file_path, "2"
    )
    port_3_mean_impedance = touchstone_tools.get_mean_port_impedance(
        file_path, "3"
    )

    plt.figure(figsize=(8,6))
    plt.plot(network.f, network.s_db[:,0,0])
    plt.plot(np.mean(network.f), np.mean(network.s_db[:,0,0]), marker="o")
    plt.show()

    print(port_1_mean_impedance)
    print(port_2_mean_impedance)
    print(port_3_mean_impedance)

    network.renormalize(
        [
            complex(port_1_mean_impedance[0], port_1_mean_impedance[1]),
            complex(port_2_mean_impedance[0], port_2_mean_impedance[1]),
            complex(port_3_mean_impedance[0], port_3_mean_impedance[1]),
        ]
    )

    plt.figure(figsize=(8,6))
    plt.plot(network.f, network.s_db[:,0,0])
    plt.plot(np.mean(network.f), np.mean(network.s_db[:, 0, 0]), marker="o")
    plt.show()

    s11_db_array = network.s_db[:,0,0]
    frequency_array = network.f
    mean_s11_db = np.mean(s11_db_array)
    half_range = (np.max(s11_db_array) - np.min(s11_db_array))/2
