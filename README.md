# microstrip_to inductor_t_junction_reflections
Package to analyse the sonnet simulation data for a niobium microstrip to aluminium T-junction. The following material parameters were used:  
1. Al: 
 * Sheet resistance sweeping from 0.1 - 1.0 Ohms/sq  
 * Kinetic inductance: 0.3 pH/sq  
2. Nb: 
 * Kinetic inductance: 0.03 pH/sq
3. SiN:
 * Thickness: 500nm
 * Relative permittivity: 7.5
 * Loss tangent: 7e-4
4. Bulk Silicon:
 * Thickness: 515nm
 * Relative permittivity: 11.5
 * Loss tangent: 0

 ## Scripts
Before running any scripts add a folder called "data" with the sonnet simulation touchstone data files of type .s3p inside.

**plot_return_loss_vs_input_width_vs_resistance.py:** Outputs a plot showing the return loss (S11) for the three port T-junction network for different niobium widths, and aluminium sheet resistances, you must specify the target inductor width.

**plot_z0_vs_linewidth.py:** Outputs two plots, the real and imaginary characterstics impedances of a niobium and an aluminium transmission line at different linewidths and aluminium resistances.
