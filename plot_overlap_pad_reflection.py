import util.touchstone_tools as touchstone_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os

directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\Inductor_microstrip_split_junction\overlap_pad_data"

plt.figure(figsize=(8, 6))

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    file_path = directory + "\\" + filename

    if ".s3p" in filename:

        # Read current touchstone file
        touchstoneFile = rf.Touchstone(file_path)
        # Get touchstone variables from comments
        model_name = filename.split("_param")[0]
        model_name = model_name.replace("microstrip_to_split_KID_inductor_deembedded_without_pad_", "").replace("_", " ")

        # Create network from touchstone file
        network = rf.Network(file_path)
        port_1_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "1"
        )
        port_2_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "2"
        )
        port_3_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "3"
        )

        network.renormalize(
            [
                complex(port_1_mean_impedance[0], port_1_mean_impedance[1]),
                complex(port_2_mean_impedance[0], port_2_mean_impedance[1]),
                complex(port_3_mean_impedance[0], port_3_mean_impedance[1]),
            ]
        )

        s11_db_array = network.s_db[:,0,0]
        mean_s11_db = np.mean(s11_db_array)
        half_range = (np.max(s11_db_array) - np.min(s11_db_array)) / 2
        print("mean s11=" + str(mean_s11_db))
        print("mean s11 err=" + str(half_range))

        plt.plot(network.f*1e-9, network.s_db[:, 0,0], label=model_name)

plt.ylabel("Return loss, S11 (dB)")
plt.xlabel("Frequency (GHz)")
plt.legend()
plt.show()
