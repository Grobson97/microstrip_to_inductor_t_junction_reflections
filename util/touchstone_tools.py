import numpy as np
import matplotlib.pyplot as plt
from lmfit import Model
from lmfit import Parameters


def get_port_parameters(string: str):
    """
    Function to extract port variables from comments in touchstone data.

    :param string: String for a given line in a comment in a touchstone file.
    :return:
    """
    pos_in_line = 0
    port_dictionary = {}

    while pos_in_line < len(string):

        # Determine index of next equals sign.
        equals_pos = string.find("=", pos_in_line, len(string))
        # If no more equals signs, break loop.
        if equals_pos == -1:
            break

        # Read back from equals sign to extract var_name
        var_name_start_pos = string.rfind(" ", pos_in_line, equals_pos)
        var_name = string[var_name_start_pos:equals_pos].strip()

        # If statement to extract value corresponding to var_name
        # If single values
        if string[equals_pos + 1] != "(":
            var_end_pos = string.find(" ", equals_pos, len(string))
            var = string[equals_pos + 1 : var_end_pos].strip()
        # If complex, i.e in a bracket.
        else:
            var_end_pos = string.find(") ", equals_pos, len(string))
            split_pos = string.find("+", equals_pos, var_end_pos)
            if split_pos == -1:
                split_pos = string.find("-", equals_pos, var_end_pos)
            var_real = float(string[equals_pos + 2 : split_pos].strip())  # First value
            var_imag = float(
                string[split_pos + 3 : var_end_pos].strip(" )")
            )  # Second value
            var = complex(var_real, var_imag)

        # Add var_name and var as key name and value to port dictionary.
        port_dictionary[var_name] = var
        # Update pos_in_line to end of var.
        pos_in_line = var_end_pos

    return port_dictionary


def get_mean_port_impedance(file_path: str, port_number: str) -> list:
    """
    Function to return the mean impedance of a given port across a frequency range from a touchstone file.
    Impedance returned in list

    :param file_path: Path to data file
    :param port_number: Port number to determine the mean impedance for.
    :return: List of impedance values in format: [r_mean, x_mean, Rerr, Xerr]
    """

    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        impedance_data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                impedance_data_points += 1

    with open(file_path, "r") as reader:
        port_impedances = {
            "F": np.empty(shape=impedance_data_points),
            "Z0": np.empty(shape=impedance_data_points, dtype=complex),
        }
        data_point = 0
        # If line starts with '!< PN' where N is the port_number, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                if "Standard port" in line:
                    print("ERROR: Port " + port_number + " information in wrong format")
                    return None
                current_port_params = get_port_parameters(line)
                port_impedances["F"][data_point] = current_port_params["F"]
                port_impedances["Z0"][data_point] = current_port_params["Z0"]

                data_point += 1

        r_mean = np.mean(port_impedances["Z0"].real)
        r_err = (
            abs(np.max(port_impedances["Z0"].real) - np.min(port_impedances["Z0"].real))
            / 2
        )
        x_mean = np.mean(port_impedances["Z0"].imag)
        x_err = (
            abs(np.max(port_impedances["Z0"].imag) - np.min(port_impedances["Z0"].imag))
            / 2
        )

    return [r_mean, x_mean, r_err, x_err]


def get_port_z0_array(file_path, port_number: str) -> np.ndarray:
    """
    Function to return an array of port Z0 values extracted from an output
    touchstone file of a sonnet simulation. These Z0 values correspond to the
    port impedance at a certain frequency. The corresponding frequencies can be
    extracted using the get_port_f_array() method.

    port_number is a string of an integer.
    """
    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_impedances = np.empty(shape=data_points, dtype=complex)
        data_point = 0
        # If line starts with '!< PN' where N is the port_number, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_impedances[data_point] = current_port_params["Z0"]

                data_point += 1

    return port_impedances


def get_port_f_array(file_path, port_number: str) -> np.ndarray:
    """
    Function to return an array of frequency values at which the port z0 and
    Eeff were evaluated in the sonnet touchstone file.

    :param file_path: Path to data file
    :param port_number: Port number to determine the mean impedance for. String of an integer.
    :return:
    """

    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_frequencies = np.empty(shape=data_points, dtype=float)
        data_point = 0
        # If line starts with '!< PN' where N is the port_number, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_frequencies[data_point] = current_port_params["F"]

                data_point += 1

    return port_frequencies


def get_port_Eeff_array(file_path: str, port_number: str) -> np.ndarray:
    """
    Function to return an array of port Eeff values extracted from an output
    touchstone file of a sonnet simulation. These Eeff values correspond to the
    port impedance at a certain frequency. The corresponding frequencies can be
    extracted using the get_port_f_array() method.

    port_number is a string of an integer.
    """
    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_permittivities = np.empty(shape=data_points, dtype=complex)
        data_point = 0
        # If line starts with '!< PN' where N is the port_number, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_permittivities[data_point] = current_port_params["Eeff"]

                data_point += 1

    return port_permittivities


def lorentzian_s21(
    frequency_array: np.array, Qr: float, Qc: float, f0: float, A=1.0
) -> np.array:
    """
    Function defining the model of a lorentzian resonance in an S21 array.

    :param np.array frequency_array: Array of N frequency points.
    :param float Qr: Resonator quality factor.
    :param float Qc: Coupling quality factor.
    :param float f0: Resonant frequency.
    :param float A: Amplitude/ background level.
    """
    x = (frequency_array - f0) / f0
    a = 2.0 * Qr * x
    real_s21 = A - (Qr / Qc) * (1.0 / (a**2 + 1.0))
    imaginary_s21 = (Qr / Qc) * (a / (a**2 + 1.0))
    magnitude_s21 = np.sqrt(real_s21**2 + imaginary_s21**2)
    return magnitude_s21


def fit_lorentzian_s21(
    frequency_array: np.array,
    s21_array: np.array,
    Qr_guess: float,
    Qc_guess: float,
    plot_graph=True,
    plot_dB=True,
    plot_title="",
) -> list:

    """
    Function to fit Qr, Qc and f0 to a resonance in S21.\n
    Returns fit parameters as a list: [Qr, Qc, f0, A].

    :param np.array frequency_array: Array of N frequency points.
    :param np.array s21_array: Complex array of N S21 data points.
    :param float Qr_guess: Guess of resonators quality factor.
    :param float Qr_guess: Guess of coupling quality factor.
    :param bool plot_graph: Boolean command to plot a graph of the fit.
    :param bool plot_dB: Boolean command to plot S21 in dB.

    """

    s21_mag_array = np.abs(s21_array)

    f0_index = np.argmin(s21_mag_array)  # index of minimum point.
    resonance_data_points = (
        300  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(lorentzian_s21)

    # Define Parameters:
    params = Parameters()
    params.add("Qc", value=Qc_guess, min=0, vary=True)
    params.add("Qr", value=Qr_guess, expr="Qc")
    params.add(
        "f0", value=frequency_array[np.argmin(s21_mag_array)], min=0, vary=True
    )  # Guesses f0 is min of s21
    params.add(
        "A", value=np.mean(np.abs(s21_array)), vary=True
    )  # Guesses background level as mean of s21.

    result = l_model.fit(
        data=np.abs(s21_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    best_fit_parameters = [
        result.best_values["Qr"],
        result.best_values["Qc"],
        result.best_values["f0"],
        result.best_values["A"],
    ]

    if plot_graph == True:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=10000,
        )

        lorentzian_fit = lorentzian_s21(
            frequency_array=f_array,
            Qr=result.best_values["Qr"],
            Qc=result.best_values["Qc"],
            f0=result.best_values["f0"],
            A=result.best_values["A"],
        )

        if plot_dB == True:
            y_label = "S21 Magnitude"
            s21_mag_array = 20 * np.log10(s21_mag_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        if plot_dB == False:
            y_label = "S21 Magnitude (dB)"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            s21_mag_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nQr=%0.2e\nQc=%0.2e\nf0=%0.2e"
            % (
                result.best_values["Qr"],
                result.best_values["Qc"],
                result.best_values["f0"],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.show()

    return best_fit_parameters


def general_lorentzian(
    frequency_array: np.array, I: float, bw: float, f0: float, c: float
) -> np.array:
    """
    Function defining the model of a general lorentzian resonance. Used for
    fitting to filter channel output S parameter. e.g. S31.
    Returns

    :param np.array frequency_array: Array of N frequency points.
    :param float A: Amplitude.
    :param float bw: Full width half maximum.
    :param float f0: Resonant frequency.
    :param float c: Offset in y.
    """
    x = (frequency_array - f0) / (0.5 * bw)
    lorentzian = I / (1 + x**2) + c
    return lorentzian


def fit_general_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    bw_guess: np.array,
    plot_graph=True,
    plot_dB=True,
    plot_title="",
) -> list:

    """
    Function to fit bw, f0, I, A to a resonance in S21.\n
    Returns fit parameters as a list: [Q, bw, f0, I, A].

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex array of N S21 data points.
    :param bw_guess: Guess for the bandwidth of the lorentzian.
    :param plot_graph: Boolean command to plot a graph of the fit.
    :param plot_dB: Boolean command to plot S Parameter in dB.

    """

    magnitude_array = np.abs(data_array)

    f0_index = np.argmax(magnitude_array)  # index of minimum point.
    resonance_data_points = (
        1000  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(general_lorentzian)

    # Define Parameters:
    params = Parameters()
    params.add("bw", value=bw_guess, vary=True)
    params.add("I", value=np.max(magnitude_array), vary=True)
    params.add("f0", value=frequency_array[np.argmax(magnitude_array)], vary=True)
    # Guesses background level as mean of s21.
    params.add("c", value=np.mean(np.abs(magnitude_array)), vary=True)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(magnitude_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    Q = result.best_values["f0"] / result.best_values["bw"]

    best_fit_parameters = [
        Q,
        result.best_values["bw"],
        result.best_values["f0"],
        result.best_values["I"],
        result.best_values["c"],
    ]

    if plot_graph == True:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=10000,
        )

        lorentzian_fit = general_lorentzian(
            frequency_array=f_array,
            I=result.best_values["I"],
            bw=result.best_values["bw"],
            f0=result.best_values["f0"],
            c=result.best_values["c"],
        )

        if plot_dB == True:
            y_label = "S21 Magnitude"
            magnitude_array = 20 * np.log10(magnitude_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        if plot_dB == False:
            y_label = "S21 Magnitude (dB)"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            magnitude_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nQ=%0.2e" % (Q),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.show()

    return best_fit_parameters


def interpolate_poly_fit(
    x_data: np.array, y_data: np.array, xi: float, degree: int, plot_graph: True
):
    """
    Function to fit a polynomial of a specified degree to x and y data and interpolate
    at a given value.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param float xi: x value at which to interpolate a y value.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.vlines(xi, np.min(y_data), np.max(y_data), color="k")
        plt.hlines(polynomial(xi), np.min(x_data), np.max(x_data), color="k")
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.show()

    return polynomial(xi)
