import util.touchstone_tools as touchstone_tools
import skrf as rf
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os

mpl.style.use('default')
plt.rcParams.update({'xtick.labelsize' : '15'})
plt.rcParams.update({'ytick.labelsize' : '15'})
plt.rcParams.update({'legend.fontsize' : '15'})
plt.rcParams.update({'font.size' : '15'})
plt.rcParams['axes.grid'] = True

# directory = r"backup"
directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\Inductor_microstrip_split_junction\extra_lossy_deembedded_data"

simulation_results = {"input_width": [], "inductor_width": [], "al_resistance": [], "nb_z0": [], "al_z0": []}

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    file_path = directory + "\\" + filename

    if ".s3p" in filename:

        # Read current touchstone file
        touchstoneFile = rf.Touchstone(file_path)
        # Get touchstone variables from comments
        variables = touchstoneFile.get_comment_variables()
        input_width = float(variables["Nb_width"][0])
        inductor_width = float(variables["inductor_width"][0])
        al_resistance = float(variables["Al_Rs"][0])

        port_1_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "1"
        )
        port_2_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "2"
        )
        port_3_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "3"
        )
        if port_1_mean_impedance is None or port_2_mean_impedance is None or port_3_mean_impedance is None:
            continue

        simulation_results["input_width"].append(input_width)
        simulation_results["inductor_width"].append(inductor_width)
        simulation_results["al_resistance"].append(al_resistance)
        simulation_results["nb_z0"].append(port_1_mean_impedance)
        simulation_results["al_z0"].append(port_2_mean_impedance)

# Convert simulation result arrays to numpy arrays:
simulation_results["input_width"] = np.array(simulation_results["input_width"])
simulation_results["inductor_width"] = np.array(simulation_results["inductor_width"])
simulation_results["al_resistance"] = np.array(simulation_results["al_resistance"])
simulation_results["nb_z0"] = np.array(simulation_results["nb_z0"])
simulation_results["al_z0"] = np.array(simulation_results["al_z0"])

# Average Nb impedance values across the same Nb linewidths:
unique_input_widths = np.unique(simulation_results["input_width"])
averaged_nb_z0_array = []
for unique_width in unique_input_widths:
    target_indices = np.where(simulation_results["input_width"] == unique_width)[0]
    nb_r_array = []
    nb_x_array = []
    nb_r_err_array = []
    nb_x_err_array = []
    for index in target_indices:
        nb_r_array.append(simulation_results["nb_z0"][index][0])
        nb_x_array.append(simulation_results["nb_z0"][index][1])
        nb_r_err_array.append(simulation_results["nb_z0"][index][2])
        nb_x_err_array.append(simulation_results["nb_z0"][index][3])

    averaged_nb_z0_array.append(
        [
            np.mean(nb_r_array),
            np.mean(nb_x_array),
            np.mean(nb_r_err_array),
            np.mean(nb_x_err_array)
        ]
    )

# Average Al impedance values across the same Al linewidths and resistances:
unique_al_resistances = np.unique(simulation_results["al_resistance"])
unique_al_widths = np.unique(simulation_results["inductor_width"])

full_averaged_al_z0_array = []  # Array containing different averaged_al_z0_arrays for each al_resistance

for unique_resistance in unique_al_resistances:
    target_resistance_indices = np.where(simulation_results["al_resistance"] == unique_resistance)[0]
    current_averaged_z0_array = []

    for unique_width in unique_al_widths:
        a = unique_width
        target_width_indices = np.where(simulation_results["inductor_width"][target_resistance_indices] == unique_width)[0]
        al_r_array = []
        al_x_array = []
        al_r_err_array = []
        al_x_err_array = []

        for width_index in target_width_indices:

            al_r_array.append(simulation_results["al_z0"][target_resistance_indices][width_index][0])
            al_x_array.append(simulation_results["al_z0"][target_resistance_indices][width_index][1])
            al_r_err_array.append(simulation_results["al_z0"][target_resistance_indices][width_index][2])
            al_x_err_array.append(simulation_results["al_z0"][target_resistance_indices][width_index][3])

        current_averaged_z0_array.append(
            [
                np.mean(al_r_array),
                np.mean(al_x_array),
                np.mean(al_r_err_array),
                np.mean(al_x_err_array)
            ]
        )

    full_averaged_al_z0_array.append(current_averaged_z0_array) # Add current z0_array to full.

full_averaged_al_z0_array = np.array(full_averaged_al_z0_array)
averaged_nb_z0_array = np.array(averaged_nb_z0_array)

# Plot real impedance
plt.figure(figsize=(8,6))
plt.errorbar(
    x=unique_input_widths,
    y=averaged_nb_z0_array[:, 0],
    yerr=averaged_nb_z0_array[:, 2],
    marker="s",
    linestyle="none",
    capsize=4,
    markersize=5,
    label="Nb"
)
for count, z0_array in enumerate(full_averaged_al_z0_array):
    plt.errorbar(
        x=unique_al_widths,
        y=z0_array[:, 0],
        yerr=z0_array[:, 2],
        marker="o",
        linestyle="none",
        capsize=4,
        markersize=5,
        label="Al - Sheet R=" + str(unique_al_resistances[count])
    )
plt.xlabel("Linewidth ($\mu$m)")
plt.ylabel("Re($Z_0$) ($\Omega$)")
plt.xlim(1.9, 6)
plt.legend()
plt.title("The real characteristic impedance of microstrip transmission\nlines with respect to the inductor input T-junction")
plt.show()

# Plot imaginary impedance
plt.figure(figsize=(8, 6))
plt.errorbar(
    x=unique_input_widths,
    y=averaged_nb_z0_array[:, 1],
    yerr=averaged_nb_z0_array[:, 3],
    marker="s",
    linestyle="none",
    capsize=4,
    markersize=5,
    label="Nb"
)
for count, z0_array in enumerate(full_averaged_al_z0_array):
    plt.errorbar(
        x=unique_al_widths,
        y=z0_array[:, 1],
        yerr=z0_array[:, 3],
        marker="o",
        linestyle="none",
        capsize=4,
        markersize=5,
        label="Al - Sheet R=" + str(unique_al_resistances[count])
    )
plt.xlabel("Linewidth ($\mu$m)")
plt.ylabel("Im($Z_0$) ($\Omega$)")
plt.xlim(1.9, 6)
plt.legend()
plt.title("The imaginary characteristic impedance of microstrip transmission\nlines with respect to the inductor input T-junction")
plt.show()

# Plot real impedance
plt.figure(figsize=(8,6))
plt.errorbar(
    x=unique_input_widths,
    y=averaged_nb_z0_array[:, 0],
    yerr=averaged_nb_z0_array[:, 2],
    marker="s",
    linestyle="none",
    capsize=4,
    markersize=5,
    label="Nb"
)
for count, z0_array in enumerate(full_averaged_al_z0_array):
    plt.errorbar(
        x=unique_al_widths,
        y=z0_array[:, 0]/2,
        yerr=z0_array[:, 2]/2,
        marker="o",
        linestyle="none",
        capsize=4,
        markersize=5,
        label="Al - Sheet R=" + str(unique_al_resistances[count])
    )
plt.xlabel("Linewidth ($\mu$m)")
plt.ylabel("Re($Z_0$) ($\Omega$)")
plt.xlim(1.9, 6)
plt.legend()
plt.title("The combined real characteristic impedance of microstrip transmission\nlines with respect to the inductor input T-junction")
plt.show()


# Plots showing the combined impedance of two al lines
# Plot imaginary impedance
plt.figure(figsize=(8, 6))
plt.errorbar(
    x=unique_input_widths,
    y=averaged_nb_z0_array[:, 1],
    yerr=averaged_nb_z0_array[:, 3],
    marker="s",
    linestyle="none",
    capsize=4,
    markersize=5,
    label="Nb"
)
for count, z0_array in enumerate(full_averaged_al_z0_array):
    plt.errorbar(
        x=unique_al_widths,
        y=z0_array[:, 1]/2,
        yerr=z0_array[:, 3]/2,
        marker="o",
        linestyle="none",
        capsize=4,
        markersize=5,
        label="Al - Sheet R=" + str(unique_al_resistances[count])
    )
plt.xlabel("Linewidth ($\mu$m)")
plt.ylabel("Im($Z_0$) ($\Omega$)")
plt.xlim(1.9, 6)
plt.legend()
plt.title("The combined imaginary characteristic impedance of microstrip transmission\nlines with respect to the inductor input T-junction")
plt.show()
