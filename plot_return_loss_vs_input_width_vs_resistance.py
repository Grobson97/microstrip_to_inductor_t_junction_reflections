import util.touchstone_tools as touchstone_tools
import skrf as rf
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os

mpl.style.use('default')
plt.rcParams.update({'xtick.labelsize' : '15'})
plt.rcParams.update({'ytick.labelsize' : '15'})
plt.rcParams.update({'legend.fontsize' : '15'})
plt.rcParams.update({'font.size' : '15'})
plt.rcParams['axes.grid'] = True

# Set the value for the inductor width to plot the data corresponding to this value.
target_inductor_width = 2.0
directory = r"data"
directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\Inductor_microstrip_split_junction\extra_lossy_deembedded_data"

simulation_results = {"input_width": [], "inductor_width": [], "al_resistance": [], "mean_s11": [], "mean_s11_err": []}

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    file_path = directory + "\\" + filename

    if ".s3p" in filename:

        # Read current touchstone file
        touchstoneFile = rf.Touchstone(file_path)
        # Get touchstone variables from comments
        variables = touchstoneFile.get_comment_variables()
        input_width = float(variables["Nb_width"][0])
        inductor_width = float(variables["inductor_width"][0])
        al_resistance = float(variables["Al_Rs"][0])

        # if input_width == 1.5 and inductor_width == 2.0 and al_resistance == 1.0:
        #     print(filename)

        # Create network from touchstone file
        network = rf.Network(file_path)

        port_1_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "1"
        )
        port_2_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "2"
        )
        port_3_mean_impedance = touchstone_tools.get_mean_port_impedance(
            file_path, "3"
        )
        if port_1_mean_impedance == None or port_2_mean_impedance == None or port_3_mean_impedance == None:
            continue

        network.renormalize(
            [
                complex(port_1_mean_impedance[0], port_1_mean_impedance[1]),
                complex(port_2_mean_impedance[0], port_2_mean_impedance[1]),
                complex(port_3_mean_impedance[0], port_3_mean_impedance[1]),
            ]
        )

        s11_db_array = network.s_db[:,0,0]
        frequency_array = network.f
        mean_s11_db = np.mean(s11_db_array)
        half_range = (np.max(s11_db_array) - np.min(s11_db_array))/2

        simulation_results["input_width"].append(input_width)
        simulation_results["inductor_width"].append(inductor_width)
        simulation_results["al_resistance"].append(al_resistance)
        simulation_results["mean_s11"].append(mean_s11_db)
        simulation_results["mean_s11_err"].append(half_range)

# Convert simulation result arrays to numpy arrays:
simulation_results["input_width"] = np.array(simulation_results["input_width"])
simulation_results["inductor_width"] = np.array(simulation_results["inductor_width"])
simulation_results["al_resistance"] = np.array(simulation_results["al_resistance"])
simulation_results["mean_s11"] = np.array(simulation_results["mean_s11"])
simulation_results["mean_s11_err"] = np.array(simulation_results["mean_s11_err"])

# Find indices corresponding to the target inductor width data.
target_inductor_width_indices = np.where(simulation_results["inductor_width"] == target_inductor_width)

# Create new arrays that only correspond to the target inductor width:
target_input_width_array = simulation_results["input_width"][target_inductor_width_indices]
target_al_resistance_array = simulation_results["al_resistance"][target_inductor_width_indices]
target_mean_s11_array = simulation_results["mean_s11"][target_inductor_width_indices]
target_mean_s11_err_array = simulation_results["mean_s11_err"][target_inductor_width_indices]


plt.figure(figsize=(8,6))
for al_resistance in np.unique(target_al_resistance_array)[1::2]:
    loop_indices = np.where(target_al_resistance_array == al_resistance)
    plt.errorbar(
        x=target_input_width_array[loop_indices],
        y=target_mean_s11_array[loop_indices],
        yerr=target_mean_s11_err_array[loop_indices],
        label=str(al_resistance) + "$\Omega$/sq",
        marker="o",
        linestyle="none",
        capsize=4,
        markersize=5
    )
plt.xlabel("Niobium input width ($\mu$m)")
plt.ylabel("Return loss, S11 (dB)")
plt.title("Finding the optimal aluminium resistance given input width and the insertion\nloss for a single Nb microstrip to a " + str(target_inductor_width) + "$\mu$m aluminium inductor\nT-junction. Assuming SiN loss tangent as 7e-4.")
plt.legend(title="Al sheet resistance:")
plt.show()
